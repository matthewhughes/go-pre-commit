# Changelog

## 0.3.0 - 2024-05-09

### Fixed

  - Fixed error error running `go` when run on a system with a different
    `GOROOT` configuration to the one the binary was built on

## 0.2.0 - 2023-08-20

### Changed

  - Make `go-mod-tidy` hook run over any change `.go` file or a changed `go.mod`

## 0.1.0 - 2023-08-20

### Added

  - Added `go-mod-tidy` `pre-commit` hook
