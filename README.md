# `go-pre-commit`

[![coverage
report](https://gitlab.com/matthewhughes/go-pre-commit/badges/main/coverage.svg)](https://gitlab.com/matthewhughes/go-pre-commit)

Some [pre-commit](pre-commit.com) hooks to run for go projects. All hooks use
the [`golang`](pre-commit.com/#golang) language in `pre-commit`, rather than
scripts that rely on Go being installed on the system

## `go-mod-tidy`

This hook runs [`go mod
tidy`](pkg.go.dev/cmd/go#hdr-Add_missing_and_remove_unused_modules) in any
directory containing a modified `go.mod` or `.go` file

``` yaml
-   repo: https://gitlab.com/matthewhughes/go-pre-commit
    rev: v0.2.0
    hooks:
    -   id: go-mod-tidy
```
