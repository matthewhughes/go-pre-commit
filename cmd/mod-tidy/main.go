package main

import (
	"bytes"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
)

func main() { //go-cov:skip
	for _, fname := range os.Args[1:] {
		if err := modTidy(fname); err != nil {
			fmt.Fprintf(os.Stderr, "%v\n", err)
			os.Exit(1)
		}
	}
}

func modTidy(fname string) error {
	var stderr bytes.Buffer
	cmd := exec.Command("go", []string{"mod", "tidy"}...)
	cmd.Stderr = &stderr
	cmd.Dir = filepath.Dir(fname)

	if err := cmd.Run(); err != nil {
		return fmt.Errorf("failed to run `go mod tidy`: %v\nstderr: %s", err, stderr.String())
	}
	return nil
}
