package main

import (
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/require"
)

const testdataDir = "testdata"

func Test_modTidyNoChangeToTidyModule(t *testing.T) {
	modPath := filepath.Join(testdataDir, "tidy-mod", "go.mod")
	require.NoError(t, modTidy(modPath))
}

func Test_modTidyChangesUntiyModule(t *testing.T) {
	untidyModuleDir := filepath.Join(testdataDir, "untidy-mod")
	modFilePath := filepath.Join(untidyModuleDir, "go.mod")
	origContent, err := os.ReadFile(modFilePath)
	require.NoError(t, err)
	expectedContent, err := os.ReadFile(filepath.Join(untidyModuleDir, "go.mod.expected"))
	require.NoError(t, err)

	defer func() {
		require.NoError(t, os.WriteFile(modFilePath, origContent, 0o600))
	}()

	require.NoError(t, modTidy(modFilePath))

	newContent, err := os.ReadFile(modFilePath)
	require.NoError(t, err)
	require.Equal(t, string(expectedContent), string(newContent))
}

func Test_modTidyReturnsErrorOnBadModule(t *testing.T) {
	badModPath := filepath.Join(t.TempDir(), "go.mod")
	expectedErrPrefix := "failed to run `go mod tidy`"

	require.ErrorContains(t, modTidy(badModPath), expectedErrPrefix)
}
